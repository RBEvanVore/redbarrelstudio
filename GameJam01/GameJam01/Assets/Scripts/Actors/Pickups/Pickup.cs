using UnityEngine;

public class Pickup : Actor
{
	public string type = "None";
	public Texture2D pickupPortrait;

	private Renderer renderer;
	protected bool wasUsed = false;
	public bool WasUsed
	{
		get{return wasUsed;}
	}

	void Awake()
	{
		SetupActor();
	}

	public virtual void OnUse()
	{
		wasUsed = true;
	}

	public void PlayOnUseEffect()
	{

	}

	public void PlayOnCollidedEffect()
	{
		
	}

	public virtual void OnDespawn()
	{
		
	}

	public virtual void VehicleReaction()
	{

	}

	public void Destroy()
	{

	}
}
