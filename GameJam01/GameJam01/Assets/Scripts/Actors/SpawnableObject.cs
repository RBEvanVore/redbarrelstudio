﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public enum SpawnableType
{
	Player,
	Enemy,
	Destructable,
	Pickup,
	Projectile,
	Tile
};

public class SpawnableObject : MonoBehaviour
{
	public delegate void SpawnableCallback(SpawnableObject spawnable);
	public event SpawnableCallback DespawnEvent;

	public SpawnableType type;

	public void OnDespawn(bool usePool = true)
	{
		if (DespawnEvent != null)
			DespawnEvent(this);	
	}
}