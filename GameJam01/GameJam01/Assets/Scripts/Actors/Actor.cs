using UnityEngine;
using System.Collections;

[RequireComponent (typeof (SphereCollider))]
[RequireComponent (typeof (Rigidbody))]
public class Actor : MonoBehaviour
{
	public virtual void SetupActor()
	{
	}
}
