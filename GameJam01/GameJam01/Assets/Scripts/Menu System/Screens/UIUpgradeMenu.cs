using System;
using UnityEngine;

public class UIUpgradeMenu : UIScreen
{
	public void EnableAutoClicker()
	{
		ClickerCounterManager.instance.EnableAutoClicker ();
	}

	public override void HandleInput ()
	{
		HandleButtonSelectionAndFiringInput();
	}
}

