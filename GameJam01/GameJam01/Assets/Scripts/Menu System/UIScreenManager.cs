using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class UIScreenManager : MonoBehaviour
{
	public float FadeInTime = 1.0f;
	public float FadeOutTime = 1.0f;

	Stack<UIScreen> displayingScreens = new Stack<UIScreen>();
	Dictionary<UIScreen, UIScreenInstantiator> screenDictionary = new Dictionary<UIScreen, UIScreenInstantiator>();
	
	public bool traceEnabled;
	public UnityEvent OnStart; 

	void Awake()
	{
		PopulateScreenTypeDictionary();
		instance = this;
	}

	void Start()
	{
		OnStart.Invoke ();
	}

	private static UIScreenManager instance;
	public static UIScreenManager Instance
	{
		get{return instance;}
	}
	
	void UnloadContent()
	{
		// Tell each of the screens to unload their content.
		foreach (UIScreen screen in displayingScreens)
		{
			screen.Unload();
		}
	}

 	void Update()
	{
		if (displayingScreens.Count > 0)
		{
			UIScreen screen = displayingScreens.Peek();
			if (screen.ScreenState == ScreenState.TransitionOn ||
		    	screen.ScreenState == ScreenState.Active)
			{
				screen.HandleInput();
			}
		}
	}
	
	void TraceScreens()
	{
		List<string> screenNames = new List<string>();
		
		foreach (UIScreen screen in displayingScreens)
			screenNames.Add(screen.GetType().Name);
		
		Debug.Log(string.Join(", ", screenNames.ToArray()));
	}

	/// <summary>
	/// Finds all the children of this transform that constain a script derived from UIScreen and adds it to the dictionary.
	/// </summary>
	void PopulateScreenTypeDictionary()
	{
		UIScreenInstantiator[] childScreens = this.transform.GetComponentsInChildren<UIScreenInstantiator>(true);
		foreach(UIScreenInstantiator instantiator in childScreens)
		{
			screenDictionary.Add(instantiator.uiPrefab, instantiator);
		}
	}

	/// <summary>
	/// Gets the type of the screen by.
	/// </summary>
	/// <returns>The screen by type.</returns>
	/// <param name="screenType">Screen type.</param>
	UIScreen GetInstantiatedScreen(UIScreen screenPrefab)
	{
		if(!screenDictionary.ContainsKey(screenPrefab))
		{
				Debug.LogError(string.Format("UIScreenManager::Cannot find screen {0}", screenPrefab.name));
			return null;
		}
		UIScreenInstantiator instantiator = screenDictionary[screenPrefab];
		return screenDictionary[screenPrefab].GetUIScreen();
	}

	/// <summary>
	/// Adds a new screen ontop of the stack without popping off the existing screen.
	/// </summary>
	/// <param name="screen">Screen.</param>
	/// <param name="controllingPlayer">Controlling player.</param>
	public void AddScreen(UIScreen screenPrefab, bool layerScreen = false)
	{
		UIScreen screen = GetInstantiatedScreen(screenPrefab);
		if (screen == null)
			return;

		//if this screen can't layer then close all the past screens
		if (!layerScreen) {
			if (displayingScreens.Count > 0)
				displayingScreens.Peek ().ExitScreen ();
		}
		
		// If we have a graphics device, tell the screen to load content.
		screen.Activate ();
		displayingScreens.Push (screen);
		
		// Print debug trace?
		if (traceEnabled)
			TraceScreens();
	}

	public void PopScreenToPrevious()
	{
		if (displayingScreens.Count > 0)
			displayingScreens.Pop().ExitScreen();

		if (displayingScreens.Count > 0)
			displayingScreens.Peek().Activate();

		// Print debug trace?
		if (traceEnabled)
			TraceScreens();
	}

	/// <summary>
	/// Pops off the top screen and pushes the new screen onto the stack.
	/// </summary>
	/// <param name="screen">Screen.</param>
	/// <param name="controllingPlayer">Controlling player.</param>
	public void ChangeScreen(UIScreen screenPrefab)
	{
		UIScreen screen = GetInstantiatedScreen(screenPrefab);
		if (screen == null)
			return;

		if (displayingScreens.Count > 0)
			displayingScreens.Pop().ExitScreen();
		AddScreen(screenPrefab);
	}

	/// <summary>
	/// Expose an array holding all the screens. We return a copy rather
	/// than the real master list, because screens should only ever be added
	/// or removed using the AddScreen and RemoveScreen methods.
	/// </summary>
	public UIScreen[] GetScreens()
	{
		return displayingScreens.ToArray();
	}

	/// <summary>
	/// Informs the screen manager to serialize its state to disk.
	/// </summary>
	public void Deactivate()
	{
		return;
	}

	public bool Activate(bool instancePreserved)
	{
		return false;
	}
}