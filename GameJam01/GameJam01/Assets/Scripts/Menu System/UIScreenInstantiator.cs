using System;
using UnityEngine;

public class UIScreenInstantiator : MonoBehaviour
{
	public UIScreen uiPrefab;
	private UIScreen instantiatedPrefab = null;

	public bool IsPrefabInstantiated
	{
		get{return instantiatedPrefab != null;}
	}

	public UIScreen GetUIScreen()
	{
 		if (!IsPrefabInstantiated)
		{
			instantiatedPrefab = Instantiate(uiPrefab) as UIScreen;
			instantiatedPrefab.transform.parent = this.transform;
			instantiatedPrefab.transform.localScale = Vector3.one;
		}
		return instantiatedPrefab;
	}
}


