using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickerCounterManager : MonoBehaviour
{

    #region Properties
    public static ClickerCounterManager instance;

    public Text m_clickCounter;

    public long m_clickCount;
    public long ClickCount
    {
        get
        {
            return this.m_clickCount;
        }
    }

    //This is the amount that is added when you do a manual click
    public long m_clickAmount = 1;
    public long ClickCountAmount
    {
        get
        {
            return this.m_clickAmount;
        }
    }

    //This is the amount that is added with auto click
    public long m_autoClickAmount = 0;
    public long AutoClickCountAmount
    {
        get
        {
            return this.m_autoClickAmount;
        }
    }

    private bool m_autoClickerEnabled = false;
    private Coroutine autoClickerRoutine;

    #endregion

    #region Monobehaviors
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            instance = this;
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        UpdateClickText();
        Debug.Log("Start AutoClicker");
        autoClickerRoutine = StartCoroutine(AutoClickerCoroutine());
    }

    void Update()
    {
    }
    #endregion

    #region Public Methods
    public void AddClick()
    {
        Debug.Log("Clicked");
        m_clickCount += m_clickAmount;
        UpdateClickText();
    }

    public void AddClickUpgrade(int cost, int clickAmount, int autoClickAmount)
    {
        Debug.Log("Bought Upgrade for " + cost + " clicks adding " + clickAmount + " to your click amount and " + autoClickAmount + " to your auto clicker");
        m_clickCount -= cost;
        m_clickAmount += clickAmount;
        m_autoClickAmount += autoClickAmount;

        UpdateClickText();
    }
    public void EnableAutoClicker()
    {
        m_autoClickerEnabled = true;
        Debug.Log("Enable Auto Click");

        Debug.Log("Start AutoClicker");
        if (autoClickerRoutine == null)
            autoClickerRoutine = StartCoroutine(AutoClickerCoroutine());
    }
    #endregion

    #region Private Methods
    private void UpdateClickText()
    {
        m_clickCounter.text = m_clickCount.ToString();
    }

    private void autoClick()
    {
        m_clickCount += m_autoClickAmount;
        UpdateClickText();
    }

    private IEnumerator AutoClickerCoroutine()
    {
        while (m_autoClickerEnabled)
        {
            yield return new WaitForSeconds(.5f);
            Debug.Log("AutoClick");
            autoClick();
        }
    }
    #endregion
}
