using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickerUpgrade : MonoBehaviour
{

    public int m_upgradeCost = 1;
    public int m_upgradeClickAmount = 0;
    public int m_upgradeAutoClickerAmount = 1;

    public void BuyUpgrade()
    {
        Debug.Log("Click Upgrade " + this.gameObject.name);

        if (ClickerCounterManager.instance.ClickCount >= m_upgradeCost)
            ClickerCounterManager.instance.AddClickUpgrade(m_upgradeCost, m_upgradeClickAmount, m_upgradeAutoClickerAmount);
        else
        {
            //Todo: Add pop up 
        }
    }
}
