﻿using UnityEngine;
using System.Collections;

public class TriggerSpawner : Spawner
{
	public float spawnInterval = 5.0f;

	void OnDisable()
	{
		StopAllCoroutines();
	}

	void OnTriggerEnter(Collider col)
	{
		StartCoroutine(SpawnCoroutine(spawnInterval));
	}

	IEnumerator SpawnCoroutine(float interval)
	{
		while (true)
		{
			yield return new WaitForSeconds(interval);

			if (curActiveSpawns < maxActiveSpawns)
				InstantiateSpawnableObject();
		}
	}
}
