﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
	public SpawnableObject prefabToSpawn;
	public float Radius;
	public Color color = Color.red;
	public int maxActiveSpawns;
	public bool spawnOfSameType = false;
	public bool useSpawnerAsParent = false;
	public int initialSpawnCount = 0;

	protected Transform cachedTransform = null;
	protected int curActiveSpawns;

	void Start()
	{
		if (cachedTransform == null)
			cachedTransform = transform;

		for (int i = 0; i < initialSpawnCount; ++i)
		{
			this.InstantiateSpawnableObject();
		}
	}

	void OnDrawGizmos()
	{
		//if (GameManager.Instance != null && GameManager.Instance.RenderLevelGizmos == false)
		//	return;

		Gizmos.color = color;
		Gizmos.DrawWireSphere(gameObject.transform.position, Radius);
	}

	protected void OnDespawnedObject(SpawnableObject spawnable)
	{
		spawnable.DespawnEvent -= OnDespawnedObject;
		--curActiveSpawns;
	}

	protected SpawnableObject InstantiateSpawnableObject()
	{
		SpawnableObject spawnable = null;
		if (spawnOfSameType == false)
			spawnable = SpawnableManager.Instance.GetActor(prefabToSpawn.name, prefabToSpawn.type.ToString(), true);
		else
			spawnable = SpawnableManager.Instance.GetActor(prefabToSpawn.type.ToString(), true);

		if (useSpawnerAsParent)
			spawnable.transform.SetParent(this.transform);

		spawnable.transform.position = new Vector3(
			Random.Range(cachedTransform.position.x + Radius, 
				cachedTransform.position.x - Radius),
			Random.Range(cachedTransform.position.y + Radius,
				cachedTransform.position.y - Radius),
			Random.Range(cachedTransform.position.z + Radius,
				cachedTransform.position.z - Radius));
		
		spawnable.transform.rotation = cachedTransform.rotation;

		spawnable.DespawnEvent += OnDespawnedObject;

		++curActiveSpawns;

		return spawnable;
	}
}