using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PoolManager<T>
{
	private List<T> objectsInPool = new List<T>();
	private List<T> objectsInScene = new List<T>();
	private GameObject poolMother;
	private GameObject activeMother;
	private bool initialized = false;

	public bool Initialized
	{
		get{return initialized;}
	}

	public List<T> ObjectsInPool
	{
		get{return objectsInPool;}
	}

	public List<T> ObjectsInScene
	{
		get{return objectsInScene;}
	}

	public GameObject PoolMother
	{
		get{return poolMother;}
	}

	public GameObject ActiveMother
	{
		get{return activeMother;}
	}

	public void Initialize()
	{
		poolMother = new GameObject(typeof(T).Name + "Pool");
		activeMother = new GameObject(typeof(T).Name + "Active");

		initialized = true;
	}

	public T PopFromPool(T obj)
	{
		T objectToReturn = FindObjectInPool(obj);
		RemoveFromPool(obj);
		return objectToReturn;
	}

	public void AddToPool(T obj)
	{
		if (obj != null && !objectsInPool.Contains(obj))
			objectsInPool.Add(obj);
	}

	public void AddToScene(T obj)
	{
		if (obj != null && !objectsInScene.Contains(obj))
			objectsInScene.Add(obj);
	}

	public void RemoveFromPool(T obj)
	{
		if (obj != null && objectsInPool.Contains(obj))
			objectsInPool.Remove(obj);
	}

	public void RemoveFromScene(T obj)
	{
		if (obj != null && objectsInScene.Contains(obj))
			objectsInScene.Remove(obj);
	}

	public T FindObjectInPool(T obj)
	{
		return objectsInPool.Find(x => obj != null && x.Equals(obj));
	}

	public T FindObjectInScene(T obj)
	{
		return objectsInScene.Find(x => obj != null && x.Equals(obj));
	}
}
