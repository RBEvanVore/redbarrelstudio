﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnableManager : MonoBehaviour
{
	private static SpawnableManager instance;
	public static SpawnableManager Instance
	{
		get{return instance;}
	}

	private PoolManager<SpawnableObject> poolManager = new PoolManager<SpawnableObject>();
	private Dictionary<string, List<SpawnableObject>> typeDictionary = new Dictionary<string, List<SpawnableObject>>();
	private Dictionary<string, GameObject> typeActiveParents = new Dictionary<string, GameObject>();
	private SpawnableObject spawnableBlueprintsParent;

	public Canvas UISpawnableParent;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		LoadBlueprints();
	}

	private void LoadBlueprints()
	{
		poolManager.Initialize();
		Object[] actors = Resources.LoadAll<GameObject>("Actors");
		for (int i = 0; i < actors.Length; ++i)
		{
			SpawnableObject actorComp = ((GameObject)actors[i]).GetComponent<SpawnableObject>();

			if (actorComp == null)
				continue;
			
			string curType = actorComp.type.ToString();

			if (typeDictionary.ContainsKey(curType) == false)
			{
				typeDictionary.Add(curType, new List<SpawnableObject>{ actorComp });
				GameObject newActiveParent = new GameObject(curType + "ActiveParent");
				newActiveParent.transform.parent = poolManager.ActiveMother.transform;
				typeActiveParents.Add(curType, newActiveParent);
			}
			else
				typeDictionary[curType].Add(actorComp);
		}
	}


	public SpawnableObject GetActor(string name, string type, bool usePool = true)
	{
		if (CheckTypeDictionary(type) == false)
			return null;

		SpawnableObject actor = InstantiateActor(type, name, usePool);

		return actor;
	}

	private bool CheckTypeDictionary(string type)
	{
		if (!typeDictionary.ContainsKey(type))
		{
			Debug.LogError("Actor system cannot load that actor, incompatable type");
			return false;
		}
		return true;
	}

	private SpawnableObject CheckPool(string name, string type)
	{
		SpawnableObject actor = null;
		actor = poolManager.ObjectsInPool.Find(x => x != null && x.name == name );
		
		if (actor != null)
		{
			poolManager.RemoveFromPool(actor);
			poolManager.AddToScene(actor);
			ChangeParent(actor, typeActiveParents[type], true);
			return actor;
		}

		return actor;
	}

	private SpawnableObject InstantiateActor(string type, string name, bool usePool)
	{
		SpawnableObject actor = null;
		SpawnableObject blueprint = null;

		if (string.IsNullOrEmpty(name))
		{
			int randomIndex = Random.Range(0, typeDictionary[type].Count);
			blueprint = typeDictionary[type][randomIndex];

			if (actor == null)
				actor = GameObject.Instantiate(blueprint);
		}
		else
		{
			blueprint = typeDictionary[type].Find(x => x.name == name);
			if (blueprint == null)
			{
				Debug.LogError("Could not spawn actor of that name");
				return null;
			}
		}

		if (usePool)
			actor = CheckPool(blueprint.name, blueprint.type.ToString());

		if (actor == null)
		{
			actor = GameObject.Instantiate(blueprint);
		}

		actor.gameObject.name = actor.gameObject.name.Replace("(Clone)", "").Trim();

		poolManager.AddToScene(actor);
		ChangeParent(actor, typeActiveParents[type], true);

		typeActiveParents[type].name = string.Format("{0}ActiveParent({1})", type, typeActiveParents[type].transform.childCount.ToString());
		poolManager.PoolMother.name = string.Format("SpawnableObjectsPool({0})", poolManager.PoolMother.transform.childCount.ToString());

		return actor;
	}

	public SpawnableObject GetActor(string type, bool usePool = true)
	{
		if (CheckTypeDictionary(type) == false)
			return null;

		SpawnableObject actor = InstantiateActor(type, null, usePool);

		return actor;
	}

	private void ChangeParent(SpawnableObject obj, GameObject parent, bool toggle)
	{
		if (obj != null)
		{
			obj.transform.SetParent(parent.transform);
			obj.gameObject.SetActive(toggle);
		}
	}

	public void RemoveActor(SpawnableObject actor, bool usePool = true)
	{
		if (actor == null)
			return;

		actor.OnDespawn();

		if (usePool)
		{
			poolManager.AddToPool(actor);
			poolManager.RemoveFromScene(actor);
			ChangeParent(actor, poolManager.PoolMother, false);
		}
		else
		{
			GameObject.Destroy(actor);
		}

		string curType = actor.type.ToString();
		typeActiveParents[curType].name = string.Format("{0}ActiveParent({1})", curType, typeActiveParents[curType].transform.childCount.ToString());
		poolManager.PoolMother.name = string.Format("SpawnableObjectsPool({0})", poolManager.PoolMother.transform.childCount.ToString());
	}
}
