using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileObject : ScriptableObject
{
    public string herbName;
    public  UnityEngine.Sprite herbImage;
    public int clickAddition;
    public int autoClickAddition;

    public float clickPercent;
    public float autoClickPercent;

    public float growingTime;
}
