using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;

public class TileManager : MonoBehaviour {

    public List<TileObject> tileObjects = new List<TileObject>();
    public List<Tile> gameTiles;

    public int totalClickAdditions = 0;
    public int totalAutoClickAddition = 0;

    public float totalClickPercent = 0;
    public float totalAutoClickPercent = 0;

    int lastTile;


	// Use this for initialization
	void Start () {
        gameTiles = new List<Tile>(GetComponentsInChildren<Tile>());
        UpdateTilesInfo();
        if( ClickerCounterManager.instance != null)
        ClickerCounterManager.instance.AddClickUpgrade( 0, totalClickAdditions, totalAutoClickAddition );
    }

    void Update()
    {
        foreach ( Tile herb in gameTiles )
        {
            if ( herb.currentGrowingTime >= herb.associatedHerb.growingTime)
            {
                if ( !herb.tileButton.IsInteractable() )
                {
                    //Set Tile as ready to harvest.
                    herb.tileButton.interactable = true;
                }
            }
            else
            {
                herb.currentGrowingTime += Time.deltaTime;
            }
        }
    }


    void UpdateTilesInfo()
    {
        totalClickAdditions = 0;
        totalAutoClickAddition = 0;
        totalClickPercent = 0;
        totalAutoClickPercent = 0;

        foreach(Tile herb in gameTiles)
        {
            herb.gameObject.name = herb.associatedHerb.herbName;
            herb.tileImage.sprite = herb.associatedHerb.herbImage;

            totalClickAdditions += herb.associatedHerb.clickAddition;
            totalAutoClickAddition += herb.associatedHerb.autoClickAddition;

            totalClickPercent += herb.associatedHerb.clickPercent;
            totalAutoClickPercent += herb.associatedHerb.autoClickPercent;
        }
    }

    void AddTile(string type)
    {
        //add new Tile image
        gameTiles[lastTile].tileImage.sprite = Resources.Load<Sprite>(type);
        //totalClickAdditions
    }

    void RemoveTile(string type)
    {
        //Remove Tile of specified type
        gameTiles[lastTile].Reset();
    }

    void CreateTileObject()
    {
        TileObject to = ScriptableObject.CreateInstance<TileObject>();
        string path = AssetDatabase.GetAssetPath( Selection.activeObject );
        if ( path == "" )
        {
            path = "Assets";
        }
        else if ( Path.GetExtension( path ) != "" )
        {
            path = path.Replace( Path.GetFileName( AssetDatabase.GetAssetPath( Selection.activeObject ) ), "" );
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath( path + "/NewTile " + ".asset" );

        AssetDatabase.CreateAsset( to, assetPathAndName );

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = to;
        tileObjects.Add( to );
    }
}
