using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour {

    public TileObject associatedHerb;
    public Button tileButton;
    public Image tileImage;

    public float currentGrowingTime = 0;

    void SetTile(TileObject herbTile)
    {
        associatedHerb = herbTile;
    }

    public void CollectTile()
    {
        tileButton.interactable = false;
        currentGrowingTime = 0;
    }

    public void Reset()
    {
        associatedHerb = null;
        tileImage = null;
        currentGrowingTime = 0;
    }
}
