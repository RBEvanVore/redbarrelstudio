﻿//------------------------------------------------------------------------------------------------
// Edy's Vehicle Physics
// (c) Angel Garcia "Edy" - Oviedo, Spain
// http://www.edy.es
//------------------------------------------------------------------------------------------------

using UnityEngine;

namespace EVP
{

public class VehicleStandardInput : MonoBehaviour
	{
	public VehicleController target;

	public bool continuousForwardAndReverse = true;
	public string steerAxis = "Horizontal";
	public string throttleAndBrakeAxis = "Vertical";
	public string handbrakeAxis = "Jump";
	public KeyCode resetVehicleKey = KeyCode.Return;

	bool m_doReset = false;

	void OnEnable ()
		{
		// Cache vehicle

		if (target == null)
			target = GetComponent<VehicleController>();
		}


	void Update ()
	{
		if (target == null) return;

		if (Input.GetKeyDown(resetVehicleKey)) m_doReset = true;
	}
	
	bool pauseInput = true;
	void OnApplicationFocus(bool focusStatus)
	{
		pauseInput = focusStatus;
	}

	void FixedUpdate ()
	{
		if (target == null) return;

		// Gather and process input

		float steerInput = Mathf.Clamp(Input.GetAxis(steerAxis), -1.0f, 1.0f);
			if (steerInput == 1.0f)
			{
				int br = 2;
			}
		float forwardInput = Mathf.Clamp01(Input.GetAxis(throttleAndBrakeAxis));
		float reverseInput = Mathf.Clamp01(-Input.GetAxis(throttleAndBrakeAxis));
		float handbrakeInput = Mathf.Clamp01(Input.GetAxis(handbrakeAxis));

			Debug.Log(forwardInput);

	/*	float steerInput = pauseInput ? Mathf.Clamp(Input.GetAxis("steer"), -1, 1) : 0.0f;
			float forwardInput = pauseInput ? Input.GetAxis("gasPedal") : 0.0f;
			float reverseInput = pauseInput ? Input.GetAxis("reverse") : 0.0f;
			float handbrakeInput = Input.GetButton("brakePedal") ? 1 : 0;*/

		float throttleInput = 0.0f;
		float brakeInput = 0.0f;

		if (continuousForwardAndReverse)
			{
			float minSpeed = 0.1f;
			float minInput = 0.1f;

			if (target.speed > minSpeed)
				{
				throttleInput = forwardInput;
				brakeInput = reverseInput;
				}
			else
				{
				if (reverseInput > minInput)
					{
						throttleInput = -reverseInput;
						brakeInput = 0.0f;
					}
				else
				if (forwardInput > minInput)
					{
					if (target.speed < -minSpeed)
						{
							throttleInput = forwardInput;
							brakeInput = 0.0f;
						}
					else
						{
						throttleInput = forwardInput;
						brakeInput = 0;
						}
					}
				}
			}
		else
			{
			//bool reverse = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);

				throttleInput = forwardInput;
				brakeInput = reverseInput;

			/*if (!reverse)
				{
				throttleInput = forwardInput;
				brakeInput = reverseInput;
				}
			else
				{
				throttleInput = -reverseInput;
				brakeInput = 0;
				}*/
			}

		// Apply input to vehicle

		target.steerInput = steerInput;

		if (!target.DisableThrottle)
		{
			target.throttleInput = throttleInput;
		}
		else
		{
			if (target.vehicleStartAudioSrc.isPlaying == false && throttleInput != 0.0f)
				target.vehicleStartAudioSrc.Play();
		}

		target.brakeInput = brakeInput;
		target.handbrakeInput = handbrakeInput;

		// Do a vehicle reset

		if (m_doReset)
			{
			target.ResetVehicle();
			m_doReset = false;
			}
		}
	}
}