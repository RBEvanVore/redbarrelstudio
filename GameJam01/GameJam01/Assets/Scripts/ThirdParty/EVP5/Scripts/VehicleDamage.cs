﻿//------------------------------------------------------------------------------------------------
// Edy's Vehicle Physics
// (c) Angel Garcia "Edy" - Oviedo, Spain
// http://www.edy.es
//------------------------------------------------------------------------------------------------

using UnityEngine;
using System.Collections.Generic;

namespace EVP
{

[RequireComponent(typeof(VehicleController))]
public class VehicleDamage : MonoBehaviour
	{
		public string newMeshName = string.Empty;

	public List<MeshFilter> meshes;
	public List<MeshCollider> colliders;
	public List<Transform> nodes;

	public bool playingSmokeEffect = false;
	public bool playingFireEffect = false;

	[Space(5)]
	public float minVelocity = 1.0f;
	public float multiplier = 1.0f;

	[Space(5)]
	public float damageRadius = 1.0f;
	public float maxDisplacement = 0.5f;
	public float maxVertexFracture = 0.1f;

	[Space(5)]
	public float nodeDamageRadius = 0.5f;
	public float maxNodeRotation = 14.0f;
	public float nodeRotationRate = 10.0f;

	[Space(5)]
	public float vertexRepairRate = 0.1f;

	public bool enableRepairKey = true;
	public KeyCode repairKey = KeyCode.R;


	VehicleController m_vehicle;
	public VehicleController Vehicle
	{
		get{return m_vehicle;}
	}

	Vector3[][] m_originalMeshes;
	Vector3[][] m_originalColliders;
	Vector3[] m_originalNodePositions;
	Quaternion[] m_originalNodeRotations;

	GameObject fireParticle;
	GameObject smokeParticle;

	bool m_repairing = false;

	public bool isRepairing { get { return m_repairing; } }

	void Awake()
	{
		nodes.RemoveAll( x => x == null );
		meshes.RemoveAll( x => x == null );
		colliders.RemoveAll( x => x == null );

		// Store original vertices of the meshes
		m_originalMeshes = new Vector3[meshes.Count][];
		for (int i = 0; i < meshes.Count; i++)
		{
			if (meshes[i] == null || meshes[i].mesh == null)
					continue;
			m_originalMeshes[i] = meshes[i].mesh.vertices;
		}

		m_originalNodePositions = new Vector3[nodes.Count];
		m_originalNodeRotations = new Quaternion[nodes.Count];
		
		for (int i = 0; i < nodes.Count; i++)
		{
			m_originalNodePositions[i] = nodes[i].transform.localPosition;
			m_originalNodeRotations[i] = nodes[i].transform.localRotation;
		}
	}


	void OnEnable ()
		{
		m_vehicle = GetComponent<VehicleController>();
		m_vehicle.processContacts = true;
		m_vehicle.onImpact += ProcessImpact;

		// Almacenar los vértices originales de los colliders a deformar

		m_originalColliders = new Vector3[colliders.Count][];
		for (int i = 0; i < colliders.Count; i++)
			m_originalColliders[i] = colliders[i].sharedMesh.vertices;

		// Almacenar posición y orientación originales de los nodos a deformar

		
		}


	void Update ()
		{
		if (enableRepairKey && Input.GetKeyDown(repairKey))
			m_repairing = true;

			// If the vehicle damage is less than 50% but greater than 25%, begin smoking
		/*	if (m_vehicle.Health < 0.5f && m_vehicle.Health > 0.25f && !playingSmokeEffect)
			{
				m_vehicle.BroadcastMessage("startFire", new HFFirePoint.particleInfo(smokeParticle, -3)); // -3 tells the system to loop indefinitely
				playingSmokeEffect = true;
			}
			else if (m_vehicle.Health < 0.25f && !playingFireEffect)
			{
				m_vehicle.BroadcastMessage("endFire");
				m_vehicle.BroadcastMessage("startFire", new HFFirePoint.particleInfo(fireParticle, -3));
				playingFireEffect = true;
			}*/

		ProcessRepair();
		}


	public void Repair ()
		{
		m_repairing = true;
		}


	//----------------------------------------------------------------------------------------------

	
	void ProcessImpact ()
		{
			if (this.enabled == false)
				return;

		Vector3 impactVelocity = Vector3.zero;

		if (m_vehicle.localImpactVelocity.sqrMagnitude > minVelocity * minVelocity)
			impactVelocity = m_vehicle.cachedTransform.TransformDirection(m_vehicle.localImpactVelocity) * multiplier * 0.02f;

		if (impactVelocity.sqrMagnitude > 0.0f)
			{
			Vector3 contactPoint = transform.TransformPoint(m_vehicle.localImpactPosition);

			// Deform the meshes

			for (int i=0, c=meshes.Count; i<c; i++)
				DeformMesh(meshes[i].mesh, m_originalMeshes[i], meshes[i].transform, contactPoint, impactVelocity);

			// Deform the colliders

			DeformColliders(contactPoint, impactVelocity);

			// Deform the nodes

			for (int i=0, c=nodes.Count; i<c; i++)
				DeformNode(nodes[i], m_originalNodePositions[i], m_originalNodeRotations[i], contactPoint, impactVelocity * 0.5f);
			}
		}


	void DeformMesh (Mesh mesh, Vector3[] originalMesh, Transform localTransform, Vector3 contactPoint, Vector3 contactVelocity)
		{
		Vector3[] vertices = mesh.vertices;
		float sqrRadius = damageRadius * damageRadius;
		float sqrMaxDeform = maxDisplacement * maxDisplacement;

		Vector3 localContactPoint = localTransform.InverseTransformPoint(contactPoint);
		Vector3 localContactForce = localTransform.InverseTransformDirection(contactVelocity);

		for (int i=0; i<vertices.Length; i++)
			{
			float dist = (localContactPoint - vertices [i]).sqrMagnitude;

			if (dist < sqrRadius)
				{
				vertices[i] += (localContactForce * (damageRadius - Mathf.Sqrt(dist)) / damageRadius) + Random.onUnitSphere * maxVertexFracture;

				Vector3 deform = vertices[i] - originalMesh[i];

				if (deform.sqrMagnitude > sqrMaxDeform)
					vertices[i] = originalMesh[i] + deform.normalized * maxDisplacement;
				}
			}

		mesh.vertices = vertices;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		}


	void DeformNode (Transform T, Vector3 originalLocalPos, Quaternion originalLocalRot, Vector3 contactPoint, Vector3 contactVelocity)
		{
		float dist = (contactPoint - T.position).sqrMagnitude;
		float damageRatio = (damageRadius - Mathf.Sqrt(dist)) / damageRadius;

		// Distort position

		if (dist < damageRadius * damageRadius)
			{
			T.position += contactVelocity * damageRatio + Random.onUnitSphere * maxVertexFracture;

			Vector3 deform = T.localPosition - originalLocalPos;

			if (deform.sqrMagnitude > maxDisplacement * maxDisplacement)
				T.localPosition = originalLocalPos + deform.normalized * maxDisplacement;
			}

		// Distort rotation

		if (dist < nodeDamageRadius * nodeDamageRadius)
			{
			Vector3 angles = AnglesToVector(T.localEulerAngles);

			Vector3 angleLimit = new Vector3(maxNodeRotation, maxNodeRotation, maxNodeRotation);
			Vector3 angleMax = angles + angleLimit;
			Vector3 angleMin = angles - angleLimit;

			angles += damageRatio * nodeRotationRate * Random.onUnitSphere;

			T.localEulerAngles = new Vector3(
				Mathf.Clamp(angles.x, angleMin.x, angleMax.x),
				Mathf.Clamp(angles.y, angleMin.y, angleMax.y),
				Mathf.Clamp(angles.z, angleMin.z, angleMax.z));
			}
		}


	Vector3 AnglesToVector (Vector3 Angles)
		{
		if (Angles.x > 180) Angles.x = -360 + Angles.x;
		if (Angles.y > 180) Angles.y = -360 + Angles.y;
		if (Angles.z > 180) Angles.z = -360 + Angles.z;
		return Angles;
		}


	void DeformColliders (Vector3 contactPoint, Vector3 impactVelocity)
		{
		// TO-DO: THIS IS NOT YET FULLY SUPPORTED IN UNITY 5 - IT MESSES UP THE WHEEL COLLIDERS
		// If the WheelColliders touch these colliders, the WheelCollider component fails
		/*
		if (colliders.Length > 0)
			{
			Vector3 CoM = m_vehicle.cachedRigidbody.centerOfMass;

			for (int i=0, c=colliders.Length; i<c; i++)
				{
				// Requires an intermediate mesh to be deformed and assigned

				Mesh mesh = new Mesh();
				mesh.vertices = colliders[i].sharedMesh.vertices;
				mesh.triangles = colliders[i].sharedMesh.triangles;

				DeformMesh(mesh, m_originalColliders[i], colliders[i].transform, contactPoint, impactVelocity);
				colliders[i].sharedMesh = mesh;
				}

			m_vehicle.cachedRigidbody.centerOfMass = CoM;
			}
		*/
		}


	//----------------------------------------------------------------------------------------------


	void ProcessRepair ()
        {
		if (m_repairing)
			{
			float repairedThreshold = 0.002f;
			bool repaired = true;

			// Move vertices towards their original positions

			for (int i=0, c=meshes.Count; i<c; i++)
				repaired = RepairMesh(meshes[i].mesh, m_originalMeshes[i], vertexRepairRate, repairedThreshold) && repaired;

			// Move nodes towards their original positions and rotations

			for (int i=0, c=nodes.Count; i<c; i++)
				repaired = RepairNode(nodes[i], m_originalNodePositions[i], m_originalNodeRotations[i], vertexRepairRate, repairedThreshold) && repaired;

			// After completing the progressive restauration, nodes and colliders are reset to
			// their exact original state.

			if (repaired)
				{
				m_repairing = false;

				// Restaurar estado exacto de los nodos

				for (int i=0, c=nodes.Count; i<c; i++)
					{
					nodes[i].localPosition = m_originalNodePositions[i];
					nodes[i].localRotation = m_originalNodeRotations[i];
					}

				// Restaurar estado exacto de los colliders

				RestoreColliders();
				}
			}
		}


	bool RepairMesh (Mesh mesh, Vector3[] originalMesh, float repairRate, float repairedThreshold)
		{
		bool result = true;
		Vector3[] vertices = mesh.vertices;

		repairRate *= Time.deltaTime;
		repairedThreshold *= repairedThreshold;  // Using squared distances

		for (int i=0, c=vertices.Length; i<c; i++)
			{
			vertices[i] = Vector3.MoveTowards(vertices[i], originalMesh[i], repairRate);

			if ((originalMesh[i] - vertices[i]).sqrMagnitude >= repairedThreshold)
				result = false;
			}

		mesh.vertices = vertices;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		return result;
		}


	bool RepairNode (Transform T, Vector3 originalLocalPosition, Quaternion originalLocalRotation, float repairRate, float repairedThreshold)
		{
		/*repairRate *= Time.deltaTime;

		T.localPosition = Vector3.MoveTowards(T.localPosition, originalLocalPosition, repairRate);
		T.localRotation = Quaternion.RotateTowards(T.localRotation, originalLocalRotation, repairRate * 50.0f);

		return (originalLocalPosition - T.localPosition).sqrMagnitude < (repairedThreshold*repairedThreshold) &&
			Quaternion.Angle(originalLocalRotation, T.localRotation) < repairedThreshold;*/
			T.localPosition = originalLocalPosition;
			T.localRotation = originalLocalRotation;
			return true;
		}


	private void RestoreColliders ()
		{
		// TO-DO: THIS IS NOT YET FULLY SUPPORTED IN UNITY 5 - IT MESSES UP THE WHEEL COLLIDERS
		// If the WheelColliders touch these colliders, the WheelCollider component fails
        /*
		if (colliders.Length > 0)
			{
			Vector3 CoM = m_vehicle.cachedRigidbody.centerOfMass;

			for (int i=0, c=colliders.Length; i<c; i++)
				{
				Mesh mesh = new Mesh();
				mesh.vertices = m_originalColliders[i];
				mesh.triangles = colliders[i].sharedMesh.triangles;

				mesh.RecalculateNormals();
				mesh.RecalculateBounds();

				colliders[i].sharedMesh = mesh;
				}

			m_vehicle.cachedRigidbody.centerOfMass = CoM;
			}
		*/
		}
	}
}