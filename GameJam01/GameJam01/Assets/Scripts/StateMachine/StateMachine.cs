﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class StateMachine : MonoBehaviour
{
	// All the states that exist in this state machine.
	public List<State> m_states = new List<State>();
	public State m_currentState = null;
	public State m_previousState = null;

	public delegate void EventHandler (string stateName);
	public event EventHandler onChangeEvents;
	public event EventHandler onLeaveEvent;
	public event EventHandler onEnterEvent;

	public void ChangeState(string stateName, string arg = "")
	{
		State newState = FindState(stateName);
		if (newState == null)
			return;
		
		if (onChangeEvents != null)
			onChangeEvents.Invoke(stateName);

		// Call the current state's leave.
		LeaveState();

		if (newState.m_onEnter != null)
		{
			m_previousState = m_currentState;
			m_currentState = newState;
			m_currentState.m_onEnter(arg);
			if (onEnterEvent != null)
				onEnterEvent.Invoke(newState.m_name);
		}
	}

	public void LeaveState(string arg = "")
	{
		if (GetCurrentStateName() == "None")
			return;
		else
		{
			if (m_currentState.m_onLeave != null) m_currentState.m_onLeave(arg);

			if (onLeaveEvent != null)
				onLeaveEvent.Invoke(m_currentState.m_name);

			m_currentState = null;
		}
	}

	private State FindState(string stateName)
	{
		foreach (State state in m_states)
		{
			if (state.m_name == stateName)
				return state;
		}

		return null;
	}

	public State AddState(string stateName, StateDelegate enter, StateDelegate update, StateDelegate leave, StateDelegate fixedUpdate = null, StateDelegate lateUpdate = null)
	{
		State newState = new State(stateName, enter, update, leave, fixedUpdate, lateUpdate);
		m_states.Add(newState);
		newState.m_stateMachine = this;
		return newState;
	}

	public void AddState(State state)
	{
		state.m_stateMachine = this;
		if (m_states.Find(x=> x.m_name == state.m_name) == null)
			m_states.Add(state);
	}

	public string GetCurrentStateName()
	{
		if (m_currentState != null)
			return m_currentState.m_name;
		else
			return "None";
	}

	public State GetStateByName(string stateName)
	{
		return m_states.Find( x => x.m_name == stateName);
	}

	public State[] GetAllPossibleStates()
	{
		return m_states.ToArray ();
	}

	void OnDestroy()
	{
		onChangeEvents = null;
		onLeaveEvent = null;
		onEnterEvent = null;
	}

	public void OnUpdate()
	{
		if (m_currentState != null && m_currentState.m_onUpdate != null)
		{
			m_currentState.m_onUpdate();
		}
	}

	public void OnLateUpdate()
	{
		if (m_currentState != null && m_currentState.m_onLateUpdate != null)
		{
			m_currentState.m_onLateUpdate();
		}
	}

	public void OnFixedUpdate()
	{
		if (m_currentState != null && m_currentState.m_onFixedUpdate != null)
		{
			m_currentState.m_onFixedUpdate();
		}
	}
}