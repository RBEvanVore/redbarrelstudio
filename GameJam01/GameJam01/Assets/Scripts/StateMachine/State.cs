﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public delegate void StateDelegate(string arg = "");

[System.Serializable]
public class State : ScriptableObject
{
	public State(string name, StateDelegate onEnter, StateDelegate onUpdate, StateDelegate onLeave, StateDelegate onFixedUpdate = null, StateDelegate lateUpdate = null)
	{
		m_name = name;
		m_onEnter = onEnter;
		m_onUpdate = onUpdate;
		m_onLateUpdate = lateUpdate;
		m_onFixedUpdate = onFixedUpdate;
		m_onLeave = onLeave;
	}
	public State(){} // If you use default constructor, make sure you set your base variables in the derived class.
	public State(object obj){}

	public void CopyUserVariables(State state)
	{
		m_name = state.m_name;
	}

	public virtual void SetupState(object obj){}

	public string m_name;

	public StateDelegate m_onEnter;
	public StateDelegate m_onUpdate;
	public StateDelegate m_onLateUpdate;
	public StateDelegate m_onFixedUpdate;
	public StateDelegate m_onLeave;
	[HideInInspector]
	public StateMachine m_stateMachine;
	public object objOfInterest;
}
