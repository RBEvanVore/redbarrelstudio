﻿//------------------------------------------------------------------------------------------------
// Edy's Vehicle Physics
// (c) Angel Garcia "Edy" - Oviedo, Spain
// http://www.edy.es
//------------------------------------------------------------------------------------------------

using UnityEngine;

namespace EVP
{

public class VehicleTelemetry : MonoBehaviour
	{
	public VehicleController target;

	public bool show = true;
	public bool gizmos = false;

	public GUIStyle style = new GUIStyle();

	string m_telemetryText = "";


	void OnEnable ()
		{
		// Cache data

		if (target == null)
			target = GetComponent<VehicleController>();

		}


	void FixedUpdate ()
		{
		if (target != null && show)
			m_telemetryText = DoTelemetry();
		}


	void Update ()
		{
		if (target != null && gizmos)
			DrawGizmos();
		}


	void OnGUI ()
		{
		if (target != null && show)
			{
			GUI.Box(new Rect (8, 8, 500, 180), "Telemetry");
			GUI.Label(new Rect (16, 28, 400, 270), m_telemetryText, style);
			}
		}


	string DoTelemetry ()
		{
		string text = string.Format("V: {0,5:0.0} m/s  {1,5:0.0} km/h {2,5:0.0} mph\n\n", target.speed, target.speed*3.6f, target.speed*2.237f);

		float downForce = 0.0f;

		foreach (WheelData wd in target.wheelData)
			text += GetWheelTelemetry(wd, ref downForce);

		text += string.Format("\n     ΣF:{0,6:0.}  Perceived mass:{1,7:0.0}\n"+
							  "               Rigidbody mass:{2,7:0.0}\n",
							  downForce, -downForce/Physics.gravity.y, target.cachedRigidbody.mass);

		if (target.debugText != "")
			text += "\n\n" + target.debugText;

		VehicleAudio vehicleAudio = target.GetComponent<VehicleAudio>();
		if (vehicleAudio != null)
			{
			text += string.Format("\nAudio gear/rpm:{0,2:0.} {1,5:0.}", vehicleAudio.simulatedGear, vehicleAudio.simulatedEngineRpm);
			}

		return text;
		}


	string GetWheelTelemetry (WheelData wd, ref float suspensionForce)
		{
		string text = string.Format("{0,-10}:{1,5:0.} rpm  ", wd.collider.gameObject.name, wd.angularVelocity * VehicleController.WToRpm);

		if (wd.grounded)
			{
			text += string.Format("C:{0,5:0.00}  F:{1,5:0.}  ", wd.suspensionCompression, wd.downforce);
			// text += string.Format("Vx:{0,6:0.00} Vy:{1,6:0.00}  ", wd.localVelocity.x, wd.localVelocity.y);
			text += string.Format("Sx:{0,6:0.00} Sy:{1,6:0.00}  ", wd.tireSlip.x, wd.tireSlip.y);
			text += string.Format("Fx:{0,5:0.} Fy:{1,5:0.}  ", wd.tireForce.x, wd.tireForce.y);

			suspensionForce += wd.hit.force;
			}
		else
			{
			text += string.Format("C: 0.--  ");
			}

		return text + "\n";
		}


	void DrawGizmos ()
		{
		CommonTools.DrawCrossMark(target.cachedTransform.TransformPoint(target.cachedRigidbody.centerOfMass), target.cachedTransform, Color.white);

		foreach (WheelData wd in target.wheelData)
			DrawWheelGizmos(wd);

		Vector3 aeroForcePoint = target.cachedTransform.TransformPoint(target.cachedRigidbody.centerOfMass + Vector3.forward * target.aeroAppPointOffset);
		CommonTools.DrawCrossMark(aeroForcePoint, target.cachedTransform, Color.cyan);
		}


	void DrawWheelGizmos (WheelData wd)
		{
		RaycastHit rayHit;
		if (wd.grounded && Physics.Raycast(wd.transform.position, -wd.transform.up, out rayHit, (wd.collider.suspensionDistance + wd.collider.radius)))
			{
			Debug.DrawLine(rayHit.point, rayHit.point + wd.transform.up * (wd.downforce / 10000.0f), wd.suspensionCompression > 0.99f? Color.magenta : Color.white);

			CommonTools.DrawCrossMark(wd.transform.position, wd.transform, Color.Lerp(Color.green, Color.gray, 0.5f));

			Vector3 forcePoint = rayHit.point + wd.transform.up * target.antiRoll * wd.forceDistance;
			CommonTools.DrawCrossMark(forcePoint, wd.transform, Color.Lerp(Color.yellow, Color.gray, 0.5f));

			Vector3 tireForce = wd.hit.forwardDir * wd.tireForce.y + wd.hit.sidewaysDir * wd.tireForce.x;
			Debug.DrawLine(forcePoint, forcePoint + CommonTools.Lin2Log(tireForce) * 0.1f, Color.green);

			Vector3 tireSlip = wd.hit.forwardDir * wd.tireSlip.y + wd.hit.sidewaysDir * wd.tireSlip.x;
			Debug.DrawLine(rayHit.point, rayHit.point + CommonTools.Lin2Log(tireSlip) * 0.5f, Color.cyan);

			// Vector3 rigForce = wd.hit.sidewaysDir * wd.localRigForce.x + wd.hit.forwardDir * wd.localRigForce.y;
			// Debug.DrawLine(rayHit.point, rayHit.point + rigForce / 10000.0f, Color.Lerp(Color.red, Color.gray, 0.5f));
			}
		}
	}
}