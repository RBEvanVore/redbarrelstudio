
Edy's Vehicle Physics for Unity 5 - BETA VERSION
http://www.edy.es/dev/vehicle-physics
------------------------------------------------

This is a beta version under development. New features and improvements are being added
regularly, as I'm porting all the features from the previous version to Unity 5.

PROJECTS USING EDY'S VEHICLE PHYSICS IN UNITY 4 WON'T UPGRADE DIRECTLY TO THIS NEW
PACKAGE IN UNITY 5.
But this new version is much better. Read the file "Upgrading from Unity 4" for details.


Two sample scenes are provided:

	The City - Simple Scene
	A single vehicle and a camera in the most simple setup.

	The City - Vehicle Manager
	Several vehicles can be selected with a vehicle manager component.


Controls:

WSAD or arrows		Thottle, brake, steering
Space				Handbrake
Enter				Reset vehicle (if it rolls over)
C					Change camera
PageUp/PageDown		Select vehicle (Vehicle Manager scene)
E					Make the gray stone to "jump" (for load tests)


Head to http://www.edy.es/dev/vehicle-physics for the component documentation.