using UnityEngine;

public class StraightFirePickup : Pickup
{
	public float speed;
	public float force;

	private Vehicle vehicle;

	public override void OnUse (Vehicle usedFromVehicle)
	{
		vehicle = usedFromVehicle;
		thisTransform = this.transform;
		this.transform.forward = vehicle.transform.forward;
		thisTransform.position = usedFromVehicle.fireMuzzle.transform.position;
		thisTransform.rotation = vehicle.transform.rotation;
		rigidBody.AddForce(vehicle.transform.forward * speed, ForceMode.Acceleration);
		base.OnUse(usedFromVehicle);
	}

	public override void OnDespawn ()
	{
		base.OnDespawn ();
	} 

	public override void VehicleReaction (Vehicle vehicle)
	{
	//	vehicle.Body.AddForce(transform.forward * force);
	}

	void Update()
	{
		ApplyGravity();
		this.transform.up = -gravityDirection;
		if (wasUsed)
		{
			//rigidBody.AddForce(transform.forward * speed);
			//transform.Translate(transform.forward * speed * Time.deltaTime);
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if (Vehicle.layerMask == collision.gameObject.layer)
		{
			PlayOnCollidedEffect();
			PickupManager.Instance.RemovePickup(this);
		}
	}
}
