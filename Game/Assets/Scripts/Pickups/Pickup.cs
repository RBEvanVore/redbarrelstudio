using UnityEngine;

public class Pickup : Actor
{
	public GameObject OnUseEffect;
	public GameObject OnHitEffect;

	public static int layerMask;
	public string type = "None";
	public Texture2D pickupPortrait;

	private Renderer renderer;
	protected bool wasUsed = false;
	public bool WasUsed
	{
		get{return wasUsed;}
	}

	void Awake()
	{
		layerMask = LayerMask.NameToLayer("Pickup");
		SetupActor();
	}

	public virtual void OnUse(Vehicle usedFromVehicle)
	{
		wasUsed = true;
	}

	public void PlayOnUseEffect()
	{

	}

	public void PlayOnCollidedEffect()
	{
		if (OnHitEffect != null)
		{
			GameObject effect = GameObject.Instantiate(OnHitEffect);
			effect.transform.position = this.transform.position;
			effect.transform.rotation = this.transform.rotation;
		}
	}

	public virtual void OnDespawn()
	{
		rigidBody.velocity = Vector3.zero;
		rigidBody.angularVelocity = Vector3.zero;
	}

	public virtual void VehicleReaction(Vehicle vehicle)
	{

	}

	public void Destroy()
	{

	}
}
