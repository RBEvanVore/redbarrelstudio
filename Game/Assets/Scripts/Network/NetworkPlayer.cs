﻿using UnityEngine;
using System;
using System.Collections;
using UnityStandardAssets.Cameras;

public class NetworkPlayer : Photon.MonoBehaviour 
{
	Vehicle vehicle;

	bool isAlive = true;
	Vector3 position;
	Quaternion rotation;
	float smoothLerp = 5.0f;

	void Awake()
	{
		vehicle = GetComponent<Vehicle>();
	}

	// Use this for initialization
	void Start ()
	{
		// Toggle necessary scripts/objects
		if (photonView.isMine)
		{
			Camera.main.GetComponent<AutoCam>().Target = vehicle.cameraTargetPos.transform;
			vehicle.InputScript.enabled = true;
		}
		else
		{
			vehicle.InputScript.enabled = false;
			StartCoroutine(Alive());
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
		}
		else
		{
			position = (Vector3)stream.ReceiveNext();
			rotation = (Quaternion)stream.ReceiveNext();
		}
	}

	IEnumerator Alive()
	{
		while(isAlive)
		{
			transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * smoothLerp);
			transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * smoothLerp);

			yield return null;
		}
	}
}
