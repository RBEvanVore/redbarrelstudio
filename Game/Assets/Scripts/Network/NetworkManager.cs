﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour 
{
	const string VERSION = "0.0.1";
	public string roomName = "PublicRoom";
	public Transform spawnPoint;

	// Use this for initialization
	void Start () 
	{
		bool connected = PhotonNetwork.ConnectUsingSettings(VERSION);
	}

	void OnJoinedLobby()
	{
		RoomOptions roomOptions = new RoomOptions(){isVisible = false, maxPlayers = 16};
		PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
	}

	void OnJoinedRoom()
	{
		PhotonNetwork.Instantiate("Vehicle",
		                          spawnPoint.position,
		                          spawnPoint.rotation,
		                          0);
	}
}
