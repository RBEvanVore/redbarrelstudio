using UnityEngine;
using System;
using System.Collections;
using EVP;
using UnityStandardAssets.Cameras;

public class Vehicle : Actor
{
	public GameObject cameraTargetPos;
	public GameObject fireMuzzle;
	public static int layerMask;

	public Pickup currentPickup;

	private VehicleStandardInput inputScript;
	public VehicleStandardInput InputScript
	{
		get{return inputScript;}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
		}
		else
		{
			position = (Vector3)stream.ReceiveNext();
			rotation = (Quaternion)stream.ReceiveNext();
		}
	}
	
	IEnumerator Alive()
	{
		while(isAlive)
		{
			transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * smoothLerp);
			transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * smoothLerp);
			
			yield return null;
		}
	}

	void Awake()
	{
		inputScript = GetComponent<VehicleStandardInput>();
	}

	void Start()
	{
		layerMask = LayerMask.NameToLayer("Vehicle");
        thisTransform = this.transform;
		SetupActor();

		if (photonView.isMine)
		{
			Camera.main.GetComponent<AutoCam>().Target = cameraTargetPos.transform;
			InputScript.enabled = true;
		}
		else
		{
			InputScript.enabled = false;
			StartCoroutine(Alive());
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if (Pickup.layerMask == collision.gameObject.layer)
		{
			Pickup pickup = collision.gameObject.GetComponent<Pickup>();
			pickup.VehicleReaction(this);
		}
	}

	void HandleInput()
	{
		if(	Input.GetButtonUp("Fire1") )
		{
			Pickup pickup = PickupManager.Instance.GetPickup(currentPickup.type);
			pickup.OnUse(this);
		}
	}

	void Update()
	{
		if (planetToOrbit != null)
			this.ApplyGravity();
		if (photonView.isMine)
		{
			HandleInput();
			Camera.main.transform.up = -gravityDirection;
			Camera.main.transform.LookAt(thisTransform.position, thisTransform.up);
		}
	}
}