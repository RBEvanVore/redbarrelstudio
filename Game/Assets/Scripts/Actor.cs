﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (SphereCollider))]
[RequireComponent (typeof (Rigidbody))]
public class Actor : Photon.MonoBehaviour 
{
	public SphereCollider sphereTrigger;
	protected Planet planetToOrbit;
	protected Transform thisTransform;
	protected Transform planetToOrbitTransform;
	protected Vector3 gravityDirection = Vector3.zero;

	// Networking variables
	protected bool isAlive = true;
	protected Vector3 position;
	protected Quaternion rotation;
	public float smoothLerp = 5.0f;

	protected Rigidbody rigidBody;
	public Rigidbody Body
	{
		get{return rigidBody;}
	}

	protected virtual void SetupActor()
	{
		if (sphereTrigger == null)
			sphereTrigger = GetComponent<SphereCollider>();
		rigidBody = GetComponent<Rigidbody>();
		sphereTrigger.isTrigger = true;
		thisTransform = this.transform;
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Planet"))
		{
			if (planetToOrbit == null || (planetToOrbit != null && planetToOrbit.gameObject != other.gameObject))
			{
				planetToOrbit = other.gameObject.GetComponent<Planet>();
				planetToOrbitTransform = other.gameObject.transform;
			}
		}
	}

	protected void ApplyGravity()
	{
		// Apply gravity
		if (rigidBody != null && planetToOrbitTransform != null)
		{
			gravityDirection = planetToOrbitTransform.position - thisTransform.position;
			gravityDirection = gravityDirection.normalized;
			rigidBody.AddForce((gravityDirection * planetToOrbit.gravity), ForceMode.Force);
		}
	}
}
