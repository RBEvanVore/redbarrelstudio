using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PickupManager : MonoBehaviour
{
	private static PickupManager instance;
	public static PickupManager Instance
	{
		get{return instance;}
	}

	private PoolManager<Pickup> poolManager = new PoolManager<Pickup>();
	private Dictionary<string, Pickup> typeDictionary;
	private GameObject pickupBlueprintsParent;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		LoadBlueprints();
	}

	private void LoadBlueprints()
	{
		poolManager.Initialize();
		Object[] pickups = Resources.LoadAll<GameObject>("Pickups");
		typeDictionary = new Dictionary<string, Pickup>();
		for (int i = 0; i < pickups.Length; ++i)
		{
			Pickup pickupComp = ((GameObject)pickups[i]).GetComponent<Pickup>();
			if (pickupComp == null)
				continue;

			if (typeDictionary.ContainsKey(pickupComp.type) == false)
				typeDictionary.Add(pickupComp.type, pickupComp);
		}
	}

	public Pickup GetPickup(string type, bool usePool = true)
	{
		if (!typeDictionary.ContainsKey(type))
		{
			Debug.LogError("Pickup system cannot load that pickup, incompatable type");
			return null;
		}

		// Check if we have one in the pool.
		Pickup pickup = null;
		if (usePool)
		{
			pickup = poolManager.ObjectsInPool.Find(x => x != null && x.type == type);
			if (pickup != null)
			{
				poolManager.RemoveFromPool(pickup);
				poolManager.AddToScene(pickup);
				ChangeParent(pickup, poolManager.ActiveMother, true);
				return pickup;
			}
		}

		// Instantiate if we haven't returned yet.
		pickup = GameObject.Instantiate(typeDictionary[type]);
		poolManager.AddToScene(pickup);
		ChangeParent(pickup, poolManager.ActiveMother, true);
		return pickup;
	}

	private void ChangeParent(Pickup obj, GameObject parent, bool toggle)
	{
		if (obj != null)
		{
			obj.transform.parent = parent.transform;
			obj.gameObject.SetActive(toggle);
		}
	}

	public void RemovePickup(Pickup pickup, bool usePool = true)
	{
		if (pickup == null)
			return;

		pickup.OnDespawn();

		if (usePool)
		{
			poolManager.AddToPool(pickup);
			poolManager.RemoveFromScene(pickup);
			ChangeParent(pickup, poolManager.PoolMother, false);
		}
		else
		{
			pickup.Destroy();
		}
	}
}