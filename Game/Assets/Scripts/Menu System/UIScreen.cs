using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

#region public enumerations
public enum ScreenState
{
	TransitionOn,
	Active,
	TransitionOff,
	Hidden,
}
#endregion

[RequireComponent(typeof(CanvasGroup))]
public class UIScreen : MonoBehaviour
{
	public List<Button> listOfButtons;
	protected int selectedIndex = 0;

	private CanvasGroup canvasGroup;

	bool isPopup = false;
	bool isSerializable = true;
	
	ScreenState screenState = ScreenState.TransitionOn;
	int? controllingPlayer;

	void Awake()
	{
		canvasGroup = GetComponent<CanvasGroup>();
		if (listOfButtons == null)
			listOfButtons = new List<Button>();
	}

	public bool IsPopup
	{
		get { return isPopup; }
		protected set { isPopup = value; }
	}
	public ScreenState ScreenState
	{
		get { return screenState; }
		protected set { screenState = value; }
	}
	public bool IsActive
	{
		get
		{
			return screenState == ScreenState.TransitionOn ||
				 screenState == ScreenState.Active;
		}
	}
	public int? ControllingPlayer
	{
		get { return controllingPlayer; }
		internal set { controllingPlayer = value; }
	}
	public bool IsSerializable
	{
		get { return isSerializable; }
		protected set { isSerializable = value; }
	}
	public virtual void Activate() 
	{
		canvasGroup.blocksRaycasts = true;
		StartCoroutine(Fade (UIScreenManager.Instance.FadeInTime, ScreenState.TransitionOn));
	}
	public virtual void Deactivate() { }
	public virtual void Unload() { }

	public virtual void OnBackButtonClicked()
	{
		UIScreenManager.Instance.PopScreenToPrevious();
		ExitScreen();
	}

	public virtual void HandleInput() { }
	public void ExitScreen()
	{
		canvasGroup.blocksRaycasts = false;
		StartCoroutine(Fade (UIScreenManager.Instance.FadeOutTime, ScreenState.TransitionOff));
	}

	protected void HandleButtonSelectionAndFiringInput()
	{
		if (Input.GetButtonUp("Vertical"))
		{
			float up = Mathf.Clamp01(Input.GetAxis("Vertical"));
			float down = Mathf.Clamp01(-Input.GetAxis("Vertical"));
			listOfButtons[selectedIndex].image.color = Color.white;
			if (up >= 0.1f)
			{
				--selectedIndex;
				if (selectedIndex < 0)
					selectedIndex = listOfButtons.Count - 1;
			}
			else if(down >= 0.1f)
			{
				++selectedIndex;
				if (selectedIndex >= listOfButtons.Count)
					selectedIndex = 0;
			}
			listOfButtons[selectedIndex].image.color = Color.yellow;
		}
		else if(Input.GetButtonUp("Fire1"))
		{
			listOfButtons[selectedIndex].onClick.Invoke();
		}
	}

	private IEnumerator Fade(float time, ScreenState screenState)
	{
		float desiredAlpha = 0.5f;
		float direction = 1.0f;
		switch(screenState)
		{
		case ScreenState.TransitionOn:
			desiredAlpha = 1.0f;
			canvasGroup.alpha = 0.0f;
			break;
		case ScreenState.TransitionOff:
			desiredAlpha = 0.0f;
			canvasGroup.alpha = 1.0f;
			direction = -1.0f;
			break;
		}

		while(canvasGroup.alpha != desiredAlpha)
		{
			canvasGroup.alpha += (Time.deltaTime / time) * direction;
			canvasGroup.alpha = Mathf.Clamp(canvasGroup.alpha, 0.0f, 1.0f);
			yield return null;
		}
	}
}
