using System;
using UnityEngine;

public class UIMainMenu : UIScreen
{
	void Start()
	{
		if (selectedIndex >= 0 && selectedIndex < listOfButtons.Count)
		{
			listOfButtons[selectedIndex].image.color = Color.yellow;
		}
	}

	public void OnSinglePlayerClicked()
	{

	}

	public void OnMultiPlayerClicked()
	{
		
	}

	public void OnOptionsClicked()
	{
		UIScreenManager.Instance.AddScreen(typeof(UIOptions), ControllingPlayer);
	}

	public override void HandleInput ()
	{
		HandleButtonSelectionAndFiringInput();
	}
}
