using UnityEngine;
using System.Collections.Generic;

public class UIScreenManager : MonoBehaviour
{
	public float FadeInTime = 1.0f;
	public float FadeOutTime = 1.0f;

	Stack<UIScreen> screens = new Stack<UIScreen>();
	Dictionary<System.Type, UIScreenInstantiator> screenTypeDictionary = new Dictionary<System.Type, UIScreenInstantiator>();
	
	public bool traceEnabled;

	void Awake()
	{
		PopulateScreenTypeDictionary();
		instance = this;

		ChangeScreen(typeof(UIMainMenu), 0);
	}

	private static UIScreenManager instance;
	public static UIScreenManager Instance
	{
		get{return instance;}
	}
	
	void UnloadContent()
	{
		// Tell each of the screens to unload their content.
		foreach (UIScreen screen in screens)
		{
			screen.Unload();
		}
	}

 	void Update()
	{
		if (screens.Count > 0)
		{
			UIScreen screen = screens.Peek();
			if (screen.ScreenState == ScreenState.TransitionOn ||
		    	screen.ScreenState == ScreenState.Active)
			{
				screen.HandleInput();
			}
		}
	}
	
	void TraceScreens()
	{
		List<string> screenNames = new List<string>();
		
		foreach (UIScreen screen in screens)
			screenNames.Add(screen.GetType().Name);
		
		Debug.Log(string.Join(", ", screenNames.ToArray()));
	}

	/// <summary>
	/// Finds all the children of this transform that constain a script derived from UIScreen and adds it to the dictionary.
	/// </summary>
	void PopulateScreenTypeDictionary()
	{
		UIScreenInstantiator[] childScreens = this.transform.GetComponentsInChildren<UIScreenInstantiator>(true);
		for(int i = 0; i < childScreens.Length; ++i)
		{
			System.Type type = childScreens[i].uiPrefab.GetType();
			screenTypeDictionary.Add(childScreens[i].uiPrefab.GetType(), childScreens[i]);
		}
	}

	/// <summary>
	/// Gets the type of the screen by.
	/// </summary>
	/// <returns>The screen by type.</returns>
	/// <param name="screenType">Screen type.</param>
	UIScreen GetScreenByType(System.Type screenType)
	{
		if (!screenTypeDictionary.ContainsKey(screenType))
		{
			Debug.LogError(string.Format("UIScreenManager::Does not contain type of {0}", screenType.ToString()));
			return null;
		}
		return screenTypeDictionary[screenType].GetUIScreen();
	}

	/// <summary>
	/// Adds a new screen ontop of the stack without popping off the existing screen.
	/// </summary>
	/// <param name="screen">Screen.</param>
	/// <param name="controllingPlayer">Controlling player.</param>
	public void AddScreen(System.Type screenType, int? controllingPlayer)
	{
		UIScreen screen = GetScreenByType(screenType);
		if (screen == null)
			return;

		if (screens.Count > 0)
			screens.Peek().ExitScreen();

		screen.ControllingPlayer = controllingPlayer;
		
		// If we have a graphics device, tell the screen to load content.
		screen.Activate ();
		screens.Push (screen);
		
		// Print debug trace?
		if (traceEnabled)
			TraceScreens();
	}

	public void PopScreenToPrevious()
	{
		if (screens.Count > 0)
			screens.Pop().ExitScreen();

		if (screens.Count > 0)
			screens.Peek().Activate();

		// Print debug trace?
		if (traceEnabled)
			TraceScreens();
	}

	/// <summary>
	/// Pops off the top screen and pushes the new screen onto the stack.
	/// </summary>
	/// <param name="screen">Screen.</param>
	/// <param name="controllingPlayer">Controlling player.</param>
	public void ChangeScreen(System.Type screenType, int? controllingPlayer)
	{
		UIScreen screen = GetScreenByType(screenType);
		if (screen == null)
			return;

		if (screens.Count > 0)
			screens.Pop().ExitScreen();
		AddScreen(screenType, controllingPlayer);
	}

	/// <summary>
	/// Expose an array holding all the screens. We return a copy rather
	/// than the real master list, because screens should only ever be added
	/// or removed using the AddScreen and RemoveScreen methods.
	/// </summary>
	public UIScreen[] GetScreens()
	{
		return screens.ToArray();
	}

	/// <summary>
	/// Informs the screen manager to serialize its state to disk.
	/// </summary>
	public void Deactivate()
	{
		return;
	}

	public bool Activate(bool instancePreserved)
	{
		return false;
	}
}